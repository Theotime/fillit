/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:06:07 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:06:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <stdio.h>
# include <stdlib.h>
# include <stddef.h>
# include "libft.h"

typedef struct s_fillit		t_fillit;
typedef struct s_item		t_item;
typedef struct s_point		t_point;

struct						s_fillit
{
	t_lst					*items;
	int						square;

	void					(*min_square)(void);
	t_item					*(*is_occuped)(int y, int x);
};

struct						s_item
{
	int						width;
	int						height;
	int						start_x;
	int						start_y;
	t_lst					*points;
	char					name;

	void					(*add_point)(t_item *item, int y, int x);
	void					(*clean)(t_item *item);
	void					(*reposition)(t_item *item);

	int						(*have_brother)(t_item *item);

	t_point					*(*get_point)(t_item *item, int y, int x);
};

struct						s_point
{
	int						x;
	int						y;
};

void						ft_fillit_init(void);
void						ft_fillit_min_square(void);
void						ft_fillit_init_fn(void);
void						ft_fillit_resolve(void);

int							ft_read_map_file(char *file);
int							ft_item_has_good_format(t_lst *lines, t_item *item);
int							ft_place_item(t_item *item, int y, int x);
int							ft_fillit_resolve_rec(t_el *cur);

t_item						*ft_item_init(void);
t_item						*ft_fillit_map_is_occuped(int y, int x);

void						ft_item_init_fn(t_item *item);
void						ft_item_add_point(t_item *item, int y, int x);
void						ft_item_clean(t_item *item);
void						ft_item_reposition(t_item *item);

int							ft_item_have_brother(t_item *item);

t_point						*ft_point_init(int y, int x);
t_point						*ft_item_get_point(t_item *item, int y, int x);

void						ft_put_item_info(t_item *item);
void						ft_put_items(t_lst *items);
void						ft_put_item(t_item *item);
void						ft_put_map(void);

char						**ft_fillit_map_init(int size);

#endif
