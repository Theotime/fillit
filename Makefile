CC=gcc

DIR_SRC=srcs
DIR_OBJ=objs

SRCS=$(shell find $(DIR_SRC) -type f)

INCLUDES=-Iinclude -Ilibft/includes

CFLAGS=-Wall -Werror -Wextra -g3 $(INCLUDES)
LDFLAGS=-Llibft -lft

NAME=fillit

OBJS=$(patsubst $(DIR_SRC)/%,$(DIR_OBJ)/%,$(SRCS:.c=.o))

all: $(NAME)

re : fclean $(NAME)

clean: libft_clean
	rm -rf $(DIR_OBJ)

fclean: libft_fclean clean
	rm -rf ${NAME}

$(NAME): $(OBJS)
	make -C libft
	$(CC) -o $@ $^ $(LDFLAGS)

$(DIR_OBJ)/%.o : $(DIR_SRC)/%.c
	mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(CFLAGS)

libft_clean:
	make clean -C libft

libft_fclean:
	make fclean -C libft

run: all

.PHONY: clean fclean re all $(NAME)
