/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:58:11 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:58:39 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_point			*ft_point_init(int y, int x)
{
	t_point		*point;
	int			len;

	len = sizeof(t_point);
	point = malloc(len);
	ft_bzero(point, len);
	point->x = x;
	point->y = y;
	return (point);
}
