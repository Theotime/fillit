/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_item_have_brother.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:55 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:57:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			have_brother(t_item *item, t_point *point)
{
	int			ret;

	ret = 0;
	if (item->get_point(item, point->y + 1, point->x))
		++ret;
	if (item->get_point(item, point->y - 1, point->x))
		++ret;
	if (item->get_point(item, point->y, point->x + 1))
		++ret;
	if (item->get_point(item, point->y, point->x - 1))
		++ret;
	return (ret);
}

int			ft_item_have_brother(t_item *item)
{
	t_el		*cur;
	int			relations;
	int			relation;

	relations = 0;
	relation = 0;
	cur = item->points->start;
	while (cur)
	{
		relation = have_brother(item, (t_point*)cur->data);
		if (relation == 0)
			return (0);
		else
			relations += relation;
		cur = cur->next;
	}
	return (relations >= 6);
}
