/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_item_has_good_format.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:53 by triviere          #+#    #+#             */
/*   Updated: 2015/12/23 16:09:02 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_parse_item_line(char *str, t_item *item, int y)
{
	int		x;

	x = 0;
	while (str[x])
	{
		if (str[x] == '#')
			item->add_point(item, y, x);
		else if (str[x] != '.')
			return (0);
		++x;
	}
	return (1);
}

int			ft_item_has_good_format(t_lst *lines, t_item *item)
{
	t_el		*cur;
	int			y;

	if (lines->len != 4)
		return (0);
	cur = lines->start;
	y = 0;
	while (cur)
	{
		if (ft_strlen((char*)cur->data) != 4 \
			|| !ft_parse_item_line((char*)cur->data, item, y))
			return (0);
		cur = cur->next;
		++y;
	}
	if (item->points->len != 4)
		return (0);
	return (1);
}
