/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_item_get_point.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:50 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:57:50 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_point		*ft_item_get_point(t_item *item, int y, int x)
{
	t_el		*cur;
	t_point		*point;

	cur = item->points->start;
	while (cur)
	{
		point = ((t_point*)cur->data);
		if (point->x == x && point->y == y)
			return (point);
		cur = cur->next;
	}
	return (NULL);
}
