/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_item_clean.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:46 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:57:47 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_item_clean(t_item *item)
{
	t_el	*cur;
	int		end_x;
	int		end_y;

	cur = item->points->start;
	item->start_x = ((t_point*)cur->data)->x;
	end_x = ((t_point*)cur->data)->x;
	item->start_y = ((t_point*)cur->data)->y;
	end_y = ((t_point*)cur->data)->y;
	while (cur)
	{
		if (((t_point*)cur->data)->x < item->start_x)
			item->start_x = ((t_point*)cur->data)->x;
		if (((t_point*)cur->data)->x > end_x)
			end_x = ((t_point*)cur->data)->x;
		if (((t_point*)cur->data)->y < item->start_y)
			item->start_y = ((t_point*)cur->data)->y;
		if (((t_point*)cur->data)->y > end_y)
			end_y = ((t_point*)cur->data)->y;
		cur = cur->next;
	}
	item->width = end_x - item->start_x + 1;
	item->height = end_y - item->start_y + 1;
	item->reposition(item);
}

void		ft_item_reposition(t_item *item)
{
	t_el		*cur;
	t_point		*tmp;

	cur = item->points->start;
	while (cur)
	{
		tmp = ((t_point*)cur->data);
		tmp->x -= item->start_x;
		tmp->y -= item->start_y;
		cur = cur->next;
	}
	item->start_x = -1;
	item->start_y = -1;
}
