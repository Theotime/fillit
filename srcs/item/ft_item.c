/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_item.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:42 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:02:55 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_item		*ft_item_init(void)
{
	t_item		*item;
	size_t		len;

	len = sizeof(t_item);
	item = malloc(len);
	ft_bzero(item, len);
	item->points = ft_lst_init();
	ft_item_init_fn(item);
	return (item);
}

void		ft_item_init_fn(t_item *item)
{
	item->add_point = &ft_item_add_point;
	item->clean = &ft_item_clean;
	item->have_brother = &ft_item_have_brother;
	item->reposition = &ft_item_reposition;
	item->get_point = &ft_item_get_point;
}

void		ft_item_add_point(t_item *item, int y, int x)
{
	t_point			*point;

	point = ft_point_init(y, x);
	ft_lst_push(item->points, ft_el_init((void*)point));
}
