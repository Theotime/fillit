/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:10 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:57:13 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_put_map(void)
{
	t_fillit	*fillit;
	t_item		*item;
	int			y;
	int			x;

	y = 0;
	fillit = ft_get_sys()->get_pgm();
	while (y < fillit->square)
	{
		x = 0;
		while (x < fillit->square)
		{
			item = fillit->is_occuped(y, x);
			if (item)
				ft_putchar(item->name);
			else
				ft_putchar('.');
			++x;
		}
		ft_putchar('\n');
		++y;
	}
}
