/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_item.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:02 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:02:37 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_put_item_info(t_item *item)
{
	t_sys		*sys;
	t_el		*cur;
	t_point		*point;

	cur = item->points->start;
	sys = ft_get_sys();
	while (cur)
	{
		point = (t_point*)cur->data;
		cur = cur->next;
	}
}

void		ft_put_item(t_item *item)
{
	int		y;
	int		x;

	y = 0;
	while (y < 4)
	{
		x = 0;
		while (x < 4)
		{
			if (item->get_point(item, y, x))
				ft_putchar(item->name);
			else
				ft_putchar(' ');
			++x;
		}
		ft_putchar('\n');
		++y;
	}
}

void		ft_put_items(t_lst *items)
{
	t_el		*cur;

	cur = items->start;
	while (cur)
	{
		ft_put_item_info((t_item*)cur->data);
		cur = cur->next;
	}
}
