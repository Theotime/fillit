/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 22:01:26 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:59:49 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			main(int ac, char **av)
{
	t_sys		*sys;
	t_fillit	*fillit;

	sys = ft_get_sys();
	sys->init(ac, av, sizeof(t_fillit));
	fillit = sys->get_pgm();
	ft_fillit_init();
	if (sys->av->len != 1 || ft_read_map_file((char*)sys->av->start->data) != 1)
		sys->die("error");
	ft_put_items(fillit->items);
	fillit->min_square();
	ft_fillit_resolve();
	ft_put_map();
	return (0);
}
