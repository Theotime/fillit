/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:23 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:57:25 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

int			ft_place_item(t_item *item, int y, int x)
{
	t_el		*cur;
	t_point		*point;

	cur = item->points->start;
	while (cur)
	{
		point = (t_point*)cur->data;
		if (ft_fillit_map_is_occuped(y + point->y, x + point->x))
			return (0);
		cur = cur->next;
	}
	return (1);
}

t_item		*ft_fillit_map_is_occuped(int y, int x)
{
	t_fillit		*fillit;
	t_el			*cur;
	t_item			*item;

	fillit = ft_get_sys()->get_pgm();
	cur = fillit->items->start;
	while (cur)
	{
		item = (t_item*)cur->data;
		if (item->start_x != -1)
		{
			if (item->start_x <= x && item->start_x + item->width > x \
				&& item->start_y <= y && item->start_y + item->height > y)
			{
				if (item->get_point(item, y - item->start_y, x - item->start_x))
					return (item);
			}
		}
		cur = cur->next;
	}
	return (NULL);
}
