/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:19 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 15:57:21 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_fillit_init(void)
{
	t_fillit		*fillit;

	fillit = ft_get_sys()->get_pgm();
	fillit->items = ft_lst_init();
	fillit->square = 0;
	ft_fillit_init_fn();
}

void		ft_fillit_init_fn(void)
{
	t_fillit		*fillit;

	fillit = ft_get_sys()->get_pgm();
	fillit->min_square = &ft_fillit_min_square;
	fillit->is_occuped = &ft_fillit_map_is_occuped;
}
