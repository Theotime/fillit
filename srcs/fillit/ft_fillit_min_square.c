/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit_min_square.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:28 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:05:02 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_fillit_min_square_by_size(t_lst *items)
{
	t_el		*cur;
	t_item		*item;
	int			n;
	int			tmp;

	cur = items->start;
	n = 0;
	while (cur)
	{
		item = ((t_item*)cur->data);
		tmp = item->width > item->height ? item->width : item->height;
		n = n < tmp ? tmp : n;
		cur = cur->next;
	}
	return (n);
}

int			ft_fillit_min_square_by_items(t_lst *items)
{
	int		elements;
	int		n;

	elements = items->len * 4;
	n = 4;
	while ((n * n) < elements)
		++n;
	return (n);
}

void		ft_fillit_min_square(void)
{
	int				size;
	t_fillit		*fillit;

	fillit = ft_get_sys()->get_pgm();
	size = ft_fillit_min_square_by_size(fillit->items);
	if ((size * size) < (fillit->items->len * 4))
		size = ft_fillit_min_square_by_items(fillit->items);
	fillit->square = size;
}
