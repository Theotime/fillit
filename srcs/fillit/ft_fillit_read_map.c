/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit_read_map.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:31 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:04:13 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_parse_item(t_lst *lines)
{
	static char		name = 'A';
	t_item			*item;
	t_fillit		*pgm;

	item = ft_item_init();
	if (!ft_item_has_good_format(lines, item))
		return (0);
	item->clean(item);
	if (!item->have_brother(item))
		return (0);
	pgm = ft_get_sys()->get_pgm();
	item->name = name++;
	ft_lst_push(pgm->items, ft_el_init((void*)item));
	return (1);
}

int			ft_read_map_file(char *file)
{
	int			fd;
	char		*line;
	t_lst		*lines;

	fd = open(file, O_RDONLY);
	lines = ft_lst_init();
	while (get_next_line(fd, &line) > 0)
	{
		if (ft_strcmp(line, ""))
			ft_lst_push(lines, ft_el_init((void*)line));
		else if (ft_parse_item(lines))
			lines = ft_lst_init();
		else
			return (0);
	}
	if (ft_parse_item(lines))
		return (1);
	else
		return (0);
}
