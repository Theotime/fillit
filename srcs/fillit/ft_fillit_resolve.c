/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit_resolve.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 15:57:35 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:03:32 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

int			ft_fillit_resolve_check(t_item *item, t_el *cur, int y, int x)
{
	item->start_x = x;
	item->start_y = y;
	if (!cur->next || ft_fillit_resolve_rec(cur->next))
		return (1);
	else
	{
		item->start_x = -1;
		item->start_y = -1;
	}
	return (0);
}

int			ft_fillit_resolve_rec(t_el *cur)
{
	t_fillit	*fillit;
	t_item		*item;
	int			y;
	int			x;

	fillit = ft_get_sys()->get_pgm();
	item = (t_item*)cur->data;
	y = 0;
	while (y < fillit->square - item->height + 1)
	{
		x = 0;
		while (x < fillit->square - item->width + 1)
		{
			if (ft_place_item(item, y, x)
				&& ft_fillit_resolve_check(item, cur, y, x))
				return (1);
			++x;
		}
		++y;
	}
	return (0);
}

void		ft_fillit_resolve(void)
{
	t_fillit	*fillit;

	fillit = ft_get_sys()->get_pgm();
	while (!ft_fillit_resolve_rec(fillit->items->start))
		++fillit->square;
}
